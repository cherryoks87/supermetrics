<?php namespace Lib;
/**
 * Main application class
 * @package Lib
 */
class App
{
	/**
	 * Run project
	 * Collect data from all reports and show it as JSON
	 */
	public static function run(): void
	{
		$reports = [
			'average_character_length_per_month_data'    => new \Reports\AverageCharacterLengthPerMonthReport(),
			'longest_post_per_month_data'                => new \Reports\LongestPostPerMonthReport(),
			'total_posts_split_by_week_number'           => new \Reports\TotalPostsSplitByWeekNumberReport(),
			'average_number_of_posts_per_user_per_month' => new \Reports\AverageNumberOfPostsPerUserPerMonthReport(),
		];
		foreach ($reports as $param_name => $report) {
			$report = $report->getData();
			$$param_name = $report;
		}
		//@TODO Add MVC
		require Config::get('templates_path', '') . 'main.php';
	}
}