<?php namespace Lib;

/**
 * Class Config
 * @package Lib
 */
class Config
{
	/**
	 * @var
	 */
	private static $config;

	/**
	 * Get value from config by the key
	 * @param string $key
	 * @param mixed  $default
	 * @return mixed|null
	 */
	public static function get(string $key, $default = null)
	{
		if (is_null(self::$config)) {
			self::$config = require(__DIR__ . '/../config/config.php');
		}

		return !empty(self::$config[$key]) ? self::$config[$key] : $default;
	}
}