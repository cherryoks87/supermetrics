<?php namespace Lib;

use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class Connector to API
 * @package Lib
 */
class Connector
{

	/** @var int Count of pages we can get from API */
	protected const PAGE_COUNT = 10;

	/** @var Client|null Client object for API */
	private $_client;

	private static $_instance;

	public static function getInstance(): Connector
	{
		if (is_null(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Connect to API, returns Client object
	 * @return Client
	 */
	protected function getClient(): Client
	{
		if (is_null($this->_client)) {
			$api_url = Config::get('api_url', '');
			$this->_client = new Client(['base_uri' => $api_url]);
		}
		return $this->_client;
	}

	/**
	 * Get token from API
	 * @return mixed
	 * @throws \Exception
	 */
	public function getToken()
	{
		try {
			$response = $this->getClient()->request('POST', 'register', [
				'json' => Config::get('api_params', [])
			]);
			if ($response->getStatusCode() === 200) {
				$body = $response->getBody();
				$result = json_decode($body);
				$token = $result->data->sl_token;
				Logger::log('The sl_token received successfully');
				return $token;
			}
		}
		catch (GuzzleException $e) {
			Logger::log('The sl_token didn\'t receive', Logger::ERR);
			Logger::log($e->getMessage());
			Logger::log(Psr7\Message::toString($e->getRequest()));
		}
	}

	/**
	 * Get all pages of posts from API
	 * @param string $token
	 * @return array
	 * @throws \Exception
	 */
	public function getData(string $token): array
	{
		$data = [];
		try {
			$promises = [];
			for ($page = 1; $page <= self::PAGE_COUNT; $page++) {
				$promises[$page] = $this->getClient()->getAsync('posts', [
					'query' => [
						'sl_token' => $token ?? '',
						'page'     => $page,
					]
				]);
			}

			$pages_with_errors = [];
			$data = $this->_checkResponse($promises, $pages_with_errors);

			/**
			 * Try again.
			 * Sometimes for some unknown reason API return error "Connection refused for URI ..."
			 * This block of code retry getting all pages with error. Usually it helps.
			 * But maybe better @TODO Add smarty retry
			 **/
			if (!empty($pages_with_errors)) {
				$promises = [];
				foreach ($pages_with_errors as $page) {
					$promises[$page] = $this->getClient()->getAsync('posts', [
						'query' => [
							'sl_token' => $token ?? '',
							'page'     => $page,
						]
					]);
				}
				$data_second_attempt = $this->_checkResponse($promises, $pages_with_errors);
				$data = array_merge($data, $data_second_attempt);
			}
			if (empty($data)) {
				throw new \Exception('Data is empty');
			}
			if (count($data) < self::PAGE_COUNT) {
				throw new \Exception('Not all pages was loaded');
			}
		}
		catch (\Exception $e) {
			Logger::log("Data from page $page didn\'t receive", Logger::ERR);
			Logger::log($e->getMessage());
			Logger::log(Psr7\Message::toString($e->getRequest()));
			Logger::log(Psr7\Message::toString($e->getResponse()));
		}
		return $data;
	}

	/**
	 * Waiting for result and check it
	 * @param array $promises
	 * @param array $pages_with_errors
	 * @return array
	 * @throws \Exception
	 */
	private function _checkResponse(array $promises, array &$pages_with_errors = []): array
	{
		$is_again = !empty($pages_with_errors);
		$data = [];
		// Wait for the requests to complete, even if some of them fail
		$responses = Promise\Utils::settle($promises)->wait();
		$pages_with_errors = [];
		foreach ($responses as $page => $response) {
			if (empty($response['value'])) {
				//Sometimes API return empty response
				Logger::log("Data from page $page return empty data "
				            . $is_again
					            ? 'again '
					            : ''
					              . implode(',', $response));
				$pages_with_errors[] = $page;
			}
			else {
				$body = $response['value']->getBody();
				$result = json_decode($body, true);
				if (!empty($result['data']['page']) && $result['data']['page'] == $page) {
					$data[] = $result['data']['posts'];
					Logger::log("Data from page $page received successfully");
				}
			}
		}
		return $data;
	}

	protected function __construct()
	{
	}

	protected function __clone()
	{
	}

	public function __wakeup()
	{
		throw new \Exception("Cannot unserialize a singleton.");
	}
}
