<?php namespace Lib;
/**
 * Class Db SQLite Connection
 * @package Lib
 */
class Db
{
	/**
	 * PDO instance
	 * @var \PDO|null
	 */
	private $_pdo;

	private static $_instance;

	public static function getInstance(): Db
	{
		if (is_null(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Connect to the SQLite database, return the PDO object
	 * @return \PDO
	 */
	public function connect(): \PDO
	{
		if (is_null($this->_pdo)) {
			try {
				$this->_pdo = new \PDO("sqlite:" . Config::get('db_path'));
			}
			catch (\PDOException $e) {
				Logger::log($e->getMessage(), Logger::ERR);
			}
			catch (\Exception $e) {
				Logger::log($e->getMessage(), Logger::ERR);
			}
		}
		return $this->_pdo;
	}

	/**
	 * Upload all posts to Sqlite Db
	 * @param array $posts
	 * @throws \Exception
	 */
	public function uploadPosts(array $posts = []): void
	{
		$pdo = $this->connect();
		//Delete old data
		$pdo->exec('DELETE FROM posts');

		//Prepare SQL query for adding rows
		$sql = 'INSERT INTO posts (id,from_name,from_id,message,type,created_time) VALUES ';
		$rows = [];
		foreach ($posts as $post) {
			$rows[] = sprintf('(\'%s\')', implode('\',\'', array_values($post)));
		}
		$sql .= implode(',', $rows);
		$pdo->exec($sql);
		$errors = $pdo->errorInfo();
		if (!empty($errors[2])) {
			throw new \Exception('Upload returned with error: ' . $errors[2]);
		}
		Logger::log('Data uploaded successful');
	}

	protected function __construct()
	{
	}

	protected function __clone()
	{
	}

	public function __wakeup()
	{
		throw new \Exception("Cannot unserialize a singleton.");
	}
}
