<html>
<head>
	<title>Supermetrics test task</title>
</head>
<body>
<h1>Reports</h1>

<h3>Average character length of posts per month</h3>
<?= $average_character_length_per_month_data->toJson() ?>

<h3>Longest post by character length per month</h3>
<?= $longest_post_per_month_data->toJson() ?>

<h3>Total posts split by week number</h3>
<?= $total_posts_split_by_week_number->toJson() ?>

<h3>Average number of posts per user per month</h3>
<?= $average_number_of_posts_per_user_per_month->toJson() ?>
</body>
</html>