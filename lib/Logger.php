<?php namespace Lib;

class Logger
{
	public const INFO = 1;
	public const ERR  = 2;

	private static $instance;

	protected static $log_name;
	protected static $log_file;

	public static function getInstance()
	{
		if (is_null(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Add message to the log
	 * @param string $message
	 * @param int    $message_type
	 * @throws \Exception
	 */
	public static function log(string $message, int $message_type = self::INFO): void
	{
		static::createLogFile();

		if (!is_resource(static::$log_name)) {
			static::$log_file = fopen(static::$log_name, 'a');
		}

		// line and file path where the log method has been executed
		$bt = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 1);
		$caller = array_shift($bt);

		$log_message = sprintf("%s[%s][%s]: %s - %s",
		                       date('Y-m-d H:i:s'),
		                       $caller['file'] ?? 'N/A',
		                       $caller['line'] ?? 'N/A',
		                       $message_type === self::INFO ? 'INFO' : 'ERR ',
		                       $message ?? 'N/A') . PHP_EOL;

		//Return output if script is running from command line
		if (self::isCli()) {
			echo $log_message;
			flush();
		}

		fwrite(static::$log_file, $log_message);

		if (static::$log_file) {
			fclose(static::$log_file);
		}
	}

	/**
	 * Create log file
	 * @throws \Exception
	 */
	public static function createLogFile(): void
	{
		$time = date('Y-m-d');
		$log_path = Config::get('log_path', '');
		static::$log_name = $log_path . "/log-{$time}.txt";

		if (!file_exists(static::$log_name)) {
			$file_name = static::$log_name;
			fopen(static::$log_name, 'w') or exit("Can't create {$file_name}!");
		}

		if (!is_writable(static::$log_name)) {
			throw new \Exception('ERROR: Unable to write to file!');
		}
	}

	/**
	 * Check if php is running from cli (command line)
	 * @return bool
	 */
	public static function isCli(): bool
	{
		if (defined('STDIN')) {
			return true;
		}

		if (empty($_SERVER['REMOTE_ADDR']) && !isset($_SERVER['HTTP_USER_AGENT']) && count($_SERVER['argv']) > 0) {
			return true;
		}

		return false;
	}

	protected function __construct()
	{
	}

	protected function __clone()
	{
	}

	public function __wakeup()
	{
		throw new \Exception("Cannot unserialize a singleton.");
	}
}