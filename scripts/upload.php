<?php
require __DIR__ . '/../vendor/autoload.php';

use Lib\Logger;
use Lib\Connector;
use Lib\Db;

try {
	$connector = Connector::getInstance();

	//1. Register a short-lived token on the fictional Supermetrics Social Network REST API
	$token = $connector->getToken();
	//Sometimes API return empty response
	if (empty($token)) {
		//Try again @TODO Add smarty retry
		$token = $connector->getToken();
	}
	if (!empty($token)) {
		//2. Fetch the posts of fictional users on a fictional social platform and process their posts
		$data = $connector->getData($token);
		$posts = array_merge(...$data);

		//Save posts to Sqlite Db
		$db = Db::getInstance();
		$db->uploadPosts($posts);
	}
}
catch (\Exception $e) {
	Logger::log($e->getMessage(), Logger::ERR);
}


