CREATE TABLE posts (
    id           TEXT      NOT NULL
        CONSTRAINT posts_id_pk
            PRIMARY KEY,
    from_name    TEXT      NOT NULL,
    from_id      TEXT      NOT NULL,
    message      TEXT      NOT NULL,
    type         TEXT,
    created_time TIMESTAMP NOT NULL
);

CREATE
UNIQUE INDEX posts_id_uindex
	ON posts(id);