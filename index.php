<?php
require __DIR__ . '/vendor/autoload.php';

use Lib\Logger;
use Lib\App;

try {
	App::run();
}
catch (\Exception $e) {
	Logger::log($e->getMessage(), Logger::ERR);
}




