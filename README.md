# Supermetrics

This project has ability to fetch the posts of fictional users on a fictional social platform from API, save it into
SqLite Db, calculate all reports and show data from them as JSON.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

For working project you have to have PHP7.x with sqlite and pdo extensions.

### Installing

1. Install php:7.x
2. Install php extensions: sqlite3 and pdo.
3. Install composer.
4. Run ```composer install``` from project root directory.

Database is located in ```db/sqlite.db```
and schema : ```scripts/db.sql```

## Testing

I haven't added tests yet, because the project doesn't have complex logic.

**But of course I vote for automatic testing by means of PHPUnit**

## How it works

To load data from API you need to go to project folder and run:

```
php scripts/upload.php
```

The script send async requests for getting all pages and return output to console and log

When you have data you can go to browser and load main page. For running web server on your localhost you can use
Built-in web server. Run this command from project root directory:

```
php -S localhost:8000
```

Then you can go to web browser and open `http://localhost:8000/`. You will see data from all 4 reports as Json.

## Project structure

~~~~
sm/                  # Root directory.
|- config/           # Folder for config file.
|- db/               # Folder for Db.
|- lib/              # All work classes.
    |- templates     # Templates for view.
    |- App.php       # Single entry point to project.
    |- Config.php    # Class for getting values from config file.
    |- Connector.php # Connector to API.
    |- Db.php        # Class for working with Db SqLite.
    |- Loger.php     # Class for generate logs.
|- logs              # Folder used to store log files.
|- reports           # Reports classes.
|- scripts           # Folder for db schema and console script for loading data.
~~~~

## Authors

* **Oksana Kukina** - *developer* - [Link](https://gitlab.com/cherryoks87)

## Acknowledgments

* For working with API I use GuzzleHttp lib, because it is simple to use it and it has a feature to send requests
  asynchronously.

## Issues and improvements

* I had a problem with API availability. The API sometimes for some unknown reason returns error
  "Connection refused for URI ...". I added the block where I retry getting this pages. It helps, but maybe it is better
  to think about the implementation of the "smart" retry.
* I made the view which is like a simple php file, but it is better to implement a proper MVC if the project will grow.
  But for this small project it has no sense.
* I implemented this project with a single page which show information from all sql queries. It is simple and fast. It
  will be good to add also Router and a few actions for each of reports. But router is a quite a big feature and
  requires to think more about URLs safety. 
  