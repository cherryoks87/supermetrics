<?php namespace Reports;

class AverageNumberOfPostsPerUserPerMonthReport extends ReportAbstract
{
	//Report's sql query
	protected $_sql = '
		SELECT from_name, COUNT(*)/COUNT(DISTINCT STRFTIME(\'%Y-%m\', created_time)) AS average_message_count
		FROM posts
		GROUP BY from_id;
		';
}