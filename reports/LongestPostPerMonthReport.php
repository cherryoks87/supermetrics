<?php namespace Reports;

class LongestPostPerMonthReport extends ReportAbstract
{
	//Report's sql query
	protected $_sql = '
		SELECT STRFTIME(\'%Y-%m\', created_time) AS MONTH, MAX(LENGTH(message)) AS max_post_length, 
			id, message, from_name, from_id, message
		FROM posts
		GROUP BY MONTH
		';
}