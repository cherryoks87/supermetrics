<?php namespace Reports;

class AverageCharacterLengthPerMonthReport extends ReportAbstract
{
	//Report's sql query
	protected $_sql = '
		SELECT STRFTIME(\'%Y-%m\', created_time) AS MONTH, AVG(LENGTH(message)) AS average_message_length
		FROM posts
		GROUP BY MONTH
		';
}