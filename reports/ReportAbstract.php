<?php namespace Reports;

use Lib\Db;

/**
 * Class ReportAbstract Abstract class for reports
 * @package Reports
 */
class ReportAbstract
{

	/**
	 * @var array|null Data array
	 */
	protected $_data;

	/**
	 * @var string Report's sql query
	 */
	protected $_sql;

	/**
	 * @return $this
	 * @throws \Exception
	 */
	public function getData(): ReportAbstract
	{
		if (empty($this->_sql)) throw new \Exception('SQL query can\'t be empty');
		$db = Db::getInstance();
		$pdo = $db->connect();
		$statement = $pdo->query($this->_sql);
		if (!$statement) {
			$errors = $pdo->errorInfo();
			$error = '';
			if (!empty($errors[2])) {
				$error = $errors[2];
			}
			throw new \Exception('Upload returned with error: ' . $error);
		}
		$this->_data = $statement->fetchAll();
		return $this;
	}

	/**
	 * Returns the JSON representation of an array of data
	 * @return false|string
	 */
	public function toJson()
	{
		return json_encode($this->_data);
	}
}