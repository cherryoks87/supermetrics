<?php namespace Reports;

class TotalPostsSplitByWeekNumberReport extends ReportAbstract
{
	//Report's sql query
	protected $_sql = '
		SELECT STRFTIME(\'%Y Week %W\', created_time) AS week, COUNT(id) AS total_posts
		FROM posts
		GROUP BY week	
		';
}