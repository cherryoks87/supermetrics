<?php
return [
	'log_path'       => __DIR__ . '/../logs',
	'templates_path' => __DIR__ . '/../lib/templates/',
	'db_path'        => 'db/sqlite.db',
	'api_url'        => 'https://api.supermetrics.com/assignment/',
	'api_params'     => [
		'client_id' => 'ju16a6m81mhid5ue1z3v2g0uh',
		'email'     => 'cherry-oks@mail.ru',
		'name'      => 'Oksana'
	]
];